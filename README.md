# titre de niveau 1
## titre de niveau 2
### titre de niveau 3

1. liste ordonnée
2. liste ordonnée suite
3. liste ordonnée encore

+ liste non ordonnée
+ liste non ordonnée suite
+ liste non ordonnée encore

** Reviron Jérôme
*slt toi*

[Google](https://www.google.fr)

|Prénom|Nom|
|---|---|
|Jérôme|Reviron|

___
ou
***
ou
---

Git est un SCM[^1] très utilisé:
[^1]: Système de gestion de versionning

![logo php](logo_php.png)

code html:

```html
<button>Cliques_moi</button>
```
code javascript:

```js
alert('hello world!');
```
